// Import dependencies
var gulp = require( 'gulp' );
var plugins = require( 'gulp-load-plugins' )( { camelize: true } );
var del = require( 'del' );
var fileExists = require( 'file-exists' );
var merge = require( 'merge-stream' );
var node = require( './package.json' );
var runSequence = require( 'run-sequence' );

// Setup plugins
plugins.del = del;
plugins.fileExists = fileExists;
plugins.merge = merge;

// Development setup
if ( ! node.production || node.production === 'false' ) {
	var browserSync = require( 'browser-sync' );

	plugins.browserSync = browserSync.create();
}

// Setup theme paths
var paths = {
	src: {
		css: './wp-content/themes/' + node.name + '/src/scss',
		fonts: './wp-content/themes/' + node.name + '/src/fonts',
		images: './wp-content/themes/' + node.name + '/src/img',
		js: './wp-content/themes/' + node.name + '/src/js',
		root: './wp-content/themes/' + node.name
	},
	dist: {
		css: './wp-content/themes/' + node.name + '/dist/css',
		fonts: './wp-content/themes/' + node.name + '/dist/fonts',
		images: './wp-content/themes/' + node.name + '/dist/img',
		js: './wp-content/themes/' + node.name + '/dist/js',
		root: './wp-content/themes/' + node.name + '/dist'
	}
};

/**
 * Clean task
 */
gulp.task( 'clean', function() {
	return plugins.del( paths.dist.root + '/**/*' );
} );

/**
 * Fonts task
 */
gulp.task( 'fonts', function() {
	var fonts = gulp.src( paths.src.fonts + '/**/*.{eot,svg,ttf,woff,woff2,otf}' )
	                .pipe( plugins.changed( paths.dist.fonts ) )
	                .pipe( gulp.dest( paths.dist.fonts ) );

	var scss = gulp.src( paths.src.fonts + '/**/*.{css,scss}' )
	               .pipe(
		               plugins.sass(
			               {
				               outputStyle: 'compressed',
				               precision: 10
			               }
		               ).on( 'error', plugins.sass.logError )
	               )
	               .pipe( plugins.autoprefixer() )
	               .pipe( plugins.size( { title: 'Fonts: Theme' } ) )
	               .pipe( plugins.changed( paths.dist.fonts ) )
	               .pipe( plugins.cssnano() )
	               .pipe( gulp.dest( paths.dist.fonts ) );

	return plugins.merge( fonts, scss );
} );

/**
 * Images task
 */
gulp.task( 'images', function() {
	return gulp.src( paths.src.images + '/**/*.{png,jpg,jpeg,gif,svg}' )
	           .pipe( plugins.changed( paths.dist.images ) )
	           .pipe(
		           plugins.imagemin(
			           {
				           progressive: true,
				           interlaced: true
			           }
		           )
	           )
	           .pipe( plugins.size( { title: 'Images: Theme' } ) )
	           .pipe( gulp.dest( paths.dist.images ) );
} );

/**
 * Scripts task
 */
gulp.task( 'scripts', function() {
	if ( ! node.production || node.production === 'false' ) {
		return gulp.src( paths.src.js + '/**/*.js' )
		           .pipe( plugins.sourcemaps.init() )
		           .pipe( plugins.include() )
		           .pipe( plugins.changed( paths.dist.js ) )
		           .pipe( plugins.jshint() )
		           .pipe( plugins.jshint.reporter( 'jshint-stylish' ) )
		           .pipe( plugins.sourcemaps.write() )
		           .pipe( plugins.size( { title: 'Javascripts: Theme' } ) )
		           .pipe( gulp.dest( paths.dist.js ) );
	}

	return gulp.src( paths.src.js + '/**/*.js' )
	           .pipe( plugins.include() )
	           .pipe( plugins.size( { title: 'Javascripts: Theme' } ) )
	           .pipe( plugins.changed( paths.dist.js ) )
	           .pipe( plugins.uglify( { preserveComments: 'some' } ) )
	           .pipe( gulp.dest( paths.dist.js ) );
} );

/**
 * Styles task
 */
gulp.task( 'styles', function() {
	if ( ! node.production || node.production === 'false' ) {
		return gulp.src( paths.src.css + '/*.scss' )
		           .pipe( plugins.sassBulkImport() )
		           .pipe( plugins.sourcemaps.init() )
		           .pipe(
			           plugins.sass(
				           {
					           outputStyle: 'compact',
					           precision: 10
				           }
			           ).on( 'error', plugins.sass.logError )
		           )
		           .pipe( plugins.autoprefixer() )
		           .pipe( plugins.size( { title: 'Styles: Theme' } ) )
		           .pipe( plugins.sourcemaps.write() )
		           .pipe( gulp.dest( paths.dist.css ) )
		           .pipe( plugins.browserSync.stream() );
	}

	return gulp.src( paths.src.css + '/*.scss' )
	           .pipe( plugins.sassBulkImport() )
	           .pipe(
		           plugins.sass(
			           {
				           outputStyle: 'compressed',
				           precision: 10
			           }
		           ).on( 'error', plugins.sass.logError )
	           )
	           .pipe( plugins.autoprefixer() )
	           .pipe( plugins.size( { title: 'Styles: Theme' } ) )
	           .pipe( plugins.changed( paths.dist.css ) )
	           .pipe( plugins.cssnano( {
		           zindex: false
	           } ) )
	           .pipe( gulp.dest( paths.dist.css ) );
} );

if ( ! node.production || node.production === 'false' ) {
	/**
	 * Reload task
	 */
	gulp.task( 'reload', function() {
		return plugins.browserSync.reload();
	} );

	/**
	 * Serve task
	 */
	gulp.task(
		'serve', [ 'styles', 'scripts', 'fonts', 'images' ], function() {
			plugins.browserSync.init(
				{
					notify: false,
					port: 3000,
					proxy: node.homepage
				}
			);

			gulp.watch( '**/*.{scss,css}', { cwd: paths.src.css }, [ 'styles' ] );
			gulp.watch( '**/*.js', { cwd: paths.src.js }, [ 'scripts', 'reload' ] );
			gulp.watch( '**/*', { cwd: paths.src.fonts }, [ 'fonts', 'reload' ] );
			gulp.watch( '**/*', { cwd: paths.src.images }, [ 'images', 'reload' ] );
			gulp.watch( '**/*.php', { cwd: paths.src.root }, [ 'reload' ] );
		}
	);
}

/**
 * Deploy task
 */
gulp.task(
	'deploy', function( callback ) {
		return runSequence(
			'clean',
			[ 'styles', 'scripts', 'fonts', 'images' ],
			callback
		);
	}
);

/**
 * Default task
 */
gulp.task(
	'default', function( callback ) {
		if ( ! node.production || node.production === 'false' ) {
			return runSequence(
				'clean',
				'serve',
				callback
			);
		} else {
			return runSequence(
				'deploy',
				callback
			);
		}
	}
);
