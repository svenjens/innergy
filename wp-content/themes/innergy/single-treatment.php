<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

	<main role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) {
			the_post();
			get_template_part( 'template-parts/content', 'banner' );
			get_template_part( 'template-parts/content', 'breadcrumbs' );
			get_template_part( 'template-parts/content', 'single_treatment' );
			get_template_part( 'template-parts/content', 'flexible' );
		} ?>
	</main>

<?php get_footer();
