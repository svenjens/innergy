<?php
/**
 * Template for displaying search forms
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */
?>

<form class="searchform" id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<fieldset>
		<input type="text" name="s" id="s" placeholder="<?php _t( 'Zoeken...' ); ?>">

		<button id="searchsubmit" type="submit"><?php _t( 'Zoek' ); ?></button>
	</fieldset>
</form>
