/**
 * Main
 *
 * The main file, page logic which is needed on all pages will be included here.
 * Please note for specific components like a slider create a separated components/<component_name>.js
 * The components will be loaded through the component loader and are not needed on every page!
 */

define( ['jquery', 'fastclick', 'mmenu', 'wow'], function( $, fast_click, wow ) {
	$( function() {
		fast_click.attach( document.body );

		$( '#nav' ).mmenu(
			{
				scrollBugFix: true,
				offCanvas: {
					position: 'right'
				},
				navbar: {
					title: ''
				}
			}, {
				clone: true
			}
		);



		$( 'a[href="#top"]' ).on(
			'click', function( event ) {
				event.preventDefault();

				$( 'html, body' ).animate( { scrollTop: 0 } );
			}
		);

		$( '.js-menu-toggle' ).click( function() {
			$( this ).toggleClass( 'is-active' );
			$( '.js-mobile' ).slideToggle();
			$( '.header__logo-white' ).toggleClass( 'header__active-white' );
			$( '.header__logo-main' ).toggleClass( 'header__active-main' );
		} );

		if ( $( window ).width() < 767 ) {
			$( '.js-toggle-mobile' ).click( function() {
				$( this ).next().slideToggle();
				$( this ).toggleClass( 'is-active' );
				$( this ).parent().siblings().find( 'p' ).slideUp();
				$( this ).parent().siblings().find( 'h6' ).removeClass( 'is-active' );
			} );
		}

		$( window ).scroll( function() {
			var scroll = $( window ).scrollTop();

			if ( scroll >= 50 ) {
				$( '.header' ).addClass( 'fixed' );
			} else {
				$( '.header' ).removeClass( 'fixed' );
			}
		} );

		$( '#play-video' ).on( 'click', function( ev ) {
			$( this ).hide();
			$( '#video' )[ 0 ].src += '&autoplay=&mute=1';
			ev.preventDefault();
		} );

		if($(window).width() > 767)
		{
			$(window).scroll(function(e){
				parallaxScroll();
			});

			function parallaxScroll(){
				var scrolled = $(window).scrollTop();
				$('#parallax-bg-1').css('top',(0-(scrolled*.1))+'px');
				$('#parallax-bg-2').css('top',(0-(scrolled*.05))+'px');
			}
		}


		// Hide Header on on scroll down
		var didScroll;
		var lastScrollTop = 0;
		var delta = 5;
		var navbarHeight = $( '.header' ).outerHeight();

		$( window ).scroll( function( event ) {
			didScroll = true;
		} );

		setInterval( function() {
			if ( didScroll ) {
				hasScrolled();
				didScroll = false;
			}
		}, 250 );

		function hasScrolled() {
			var st = $( this ).scrollTop();

			// Make sure they scroll more than delta
			if ( Math.abs( lastScrollTop - st ) <= delta ) {
				return;
			}

			// If they scrolled down and are past the navbar, add class .nav-up.
			// This is necessary so you never see what is "behind" the navbar.
			if ( st > lastScrollTop && st > navbarHeight ) {
				// Scroll Down
				$( 'header' ).removeClass( 'nav-down' ).addClass( 'nav-up' );
			} else {
				// Scroll Up
				if ( st + $( window ).height() < $( document ).height() ) {
					$( 'header' ).removeClass( 'nav-up' ).addClass( 'nav-down' );
				}
			}

			lastScrollTop = st;
		}

		wow = new WOW(
			{
				animateClass: 'animated',
				offset: 100
			}
		);
		wow.init();

	} );
} );
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJhcHAvbWFpbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIE1haW5cbiAqXG4gKiBUaGUgbWFpbiBmaWxlLCBwYWdlIGxvZ2ljIHdoaWNoIGlzIG5lZWRlZCBvbiBhbGwgcGFnZXMgd2lsbCBiZSBpbmNsdWRlZCBoZXJlLlxuICogUGxlYXNlIG5vdGUgZm9yIHNwZWNpZmljIGNvbXBvbmVudHMgbGlrZSBhIHNsaWRlciBjcmVhdGUgYSBzZXBhcmF0ZWQgY29tcG9uZW50cy88Y29tcG9uZW50X25hbWU+LmpzXG4gKiBUaGUgY29tcG9uZW50cyB3aWxsIGJlIGxvYWRlZCB0aHJvdWdoIHRoZSBjb21wb25lbnQgbG9hZGVyIGFuZCBhcmUgbm90IG5lZWRlZCBvbiBldmVyeSBwYWdlIVxuICovXG5cbmRlZmluZSggWydqcXVlcnknLCAnZmFzdGNsaWNrJywgJ21tZW51JywgJ3dvdyddLCBmdW5jdGlvbiggJCwgZmFzdF9jbGljaywgd293ICkge1xuXHQkKCBmdW5jdGlvbigpIHtcblx0XHRmYXN0X2NsaWNrLmF0dGFjaCggZG9jdW1lbnQuYm9keSApO1xuXG5cdFx0JCggJyNuYXYnICkubW1lbnUoXG5cdFx0XHR7XG5cdFx0XHRcdHNjcm9sbEJ1Z0ZpeDogdHJ1ZSxcblx0XHRcdFx0b2ZmQ2FudmFzOiB7XG5cdFx0XHRcdFx0cG9zaXRpb246ICdyaWdodCdcblx0XHRcdFx0fSxcblx0XHRcdFx0bmF2YmFyOiB7XG5cdFx0XHRcdFx0dGl0bGU6ICcnXG5cdFx0XHRcdH1cblx0XHRcdH0sIHtcblx0XHRcdFx0Y2xvbmU6IHRydWVcblx0XHRcdH1cblx0XHQpO1xuXG5cblxuXHRcdCQoICdhW2hyZWY9XCIjdG9wXCJdJyApLm9uKFxuXHRcdFx0J2NsaWNrJywgZnVuY3Rpb24oIGV2ZW50ICkge1xuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cdFx0XHRcdCQoICdodG1sLCBib2R5JyApLmFuaW1hdGUoIHsgc2Nyb2xsVG9wOiAwIH0gKTtcblx0XHRcdH1cblx0XHQpO1xuXG5cdFx0JCggJy5qcy1tZW51LXRvZ2dsZScgKS5jbGljayggZnVuY3Rpb24oKSB7XG5cdFx0XHQkKCB0aGlzICkudG9nZ2xlQ2xhc3MoICdpcy1hY3RpdmUnICk7XG5cdFx0XHQkKCAnLmpzLW1vYmlsZScgKS5zbGlkZVRvZ2dsZSgpO1xuXHRcdFx0JCggJy5oZWFkZXJfX2xvZ28td2hpdGUnICkudG9nZ2xlQ2xhc3MoICdoZWFkZXJfX2FjdGl2ZS13aGl0ZScgKTtcblx0XHRcdCQoICcuaGVhZGVyX19sb2dvLW1haW4nICkudG9nZ2xlQ2xhc3MoICdoZWFkZXJfX2FjdGl2ZS1tYWluJyApO1xuXHRcdH0gKTtcblxuXHRcdGlmICggJCggd2luZG93ICkud2lkdGgoKSA8IDc2NyApIHtcblx0XHRcdCQoICcuanMtdG9nZ2xlLW1vYmlsZScgKS5jbGljayggZnVuY3Rpb24oKSB7XG5cdFx0XHRcdCQoIHRoaXMgKS5uZXh0KCkuc2xpZGVUb2dnbGUoKTtcblx0XHRcdFx0JCggdGhpcyApLnRvZ2dsZUNsYXNzKCAnaXMtYWN0aXZlJyApO1xuXHRcdFx0XHQkKCB0aGlzICkucGFyZW50KCkuc2libGluZ3MoKS5maW5kKCAncCcgKS5zbGlkZVVwKCk7XG5cdFx0XHRcdCQoIHRoaXMgKS5wYXJlbnQoKS5zaWJsaW5ncygpLmZpbmQoICdoNicgKS5yZW1vdmVDbGFzcyggJ2lzLWFjdGl2ZScgKTtcblx0XHRcdH0gKTtcblx0XHR9XG5cblx0XHQkKCB3aW5kb3cgKS5zY3JvbGwoIGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIHNjcm9sbCA9ICQoIHdpbmRvdyApLnNjcm9sbFRvcCgpO1xuXG5cdFx0XHRpZiAoIHNjcm9sbCA+PSA1MCApIHtcblx0XHRcdFx0JCggJy5oZWFkZXInICkuYWRkQ2xhc3MoICdmaXhlZCcgKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCQoICcuaGVhZGVyJyApLnJlbW92ZUNsYXNzKCAnZml4ZWQnICk7XG5cdFx0XHR9XG5cdFx0fSApO1xuXG5cdFx0JCggJyNwbGF5LXZpZGVvJyApLm9uKCAnY2xpY2snLCBmdW5jdGlvbiggZXYgKSB7XG5cdFx0XHQkKCB0aGlzICkuaGlkZSgpO1xuXHRcdFx0JCggJyN2aWRlbycgKVsgMCBdLnNyYyArPSAnJmF1dG9wbGF5PSZtdXRlPTEnO1xuXHRcdFx0ZXYucHJldmVudERlZmF1bHQoKTtcblx0XHR9ICk7XG5cblx0XHRpZigkKHdpbmRvdykud2lkdGgoKSA+IDc2Nylcblx0XHR7XG5cdFx0XHQkKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uKGUpe1xuXHRcdFx0XHRwYXJhbGxheFNjcm9sbCgpO1xuXHRcdFx0fSk7XG5cblx0XHRcdGZ1bmN0aW9uIHBhcmFsbGF4U2Nyb2xsKCl7XG5cdFx0XHRcdHZhciBzY3JvbGxlZCA9ICQod2luZG93KS5zY3JvbGxUb3AoKTtcblx0XHRcdFx0JCgnI3BhcmFsbGF4LWJnLTEnKS5jc3MoJ3RvcCcsKDAtKHNjcm9sbGVkKi4xKSkrJ3B4Jyk7XG5cdFx0XHRcdCQoJyNwYXJhbGxheC1iZy0yJykuY3NzKCd0b3AnLCgwLShzY3JvbGxlZCouMDUpKSsncHgnKTtcblx0XHRcdH1cblx0XHR9XG5cblxuXHRcdC8vIEhpZGUgSGVhZGVyIG9uIG9uIHNjcm9sbCBkb3duXG5cdFx0dmFyIGRpZFNjcm9sbDtcblx0XHR2YXIgbGFzdFNjcm9sbFRvcCA9IDA7XG5cdFx0dmFyIGRlbHRhID0gNTtcblx0XHR2YXIgbmF2YmFySGVpZ2h0ID0gJCggJy5oZWFkZXInICkub3V0ZXJIZWlnaHQoKTtcblxuXHRcdCQoIHdpbmRvdyApLnNjcm9sbCggZnVuY3Rpb24oIGV2ZW50ICkge1xuXHRcdFx0ZGlkU2Nyb2xsID0gdHJ1ZTtcblx0XHR9ICk7XG5cblx0XHRzZXRJbnRlcnZhbCggZnVuY3Rpb24oKSB7XG5cdFx0XHRpZiAoIGRpZFNjcm9sbCApIHtcblx0XHRcdFx0aGFzU2Nyb2xsZWQoKTtcblx0XHRcdFx0ZGlkU2Nyb2xsID0gZmFsc2U7XG5cdFx0XHR9XG5cdFx0fSwgMjUwICk7XG5cblx0XHRmdW5jdGlvbiBoYXNTY3JvbGxlZCgpIHtcblx0XHRcdHZhciBzdCA9ICQoIHRoaXMgKS5zY3JvbGxUb3AoKTtcblxuXHRcdFx0Ly8gTWFrZSBzdXJlIHRoZXkgc2Nyb2xsIG1vcmUgdGhhbiBkZWx0YVxuXHRcdFx0aWYgKCBNYXRoLmFicyggbGFzdFNjcm9sbFRvcCAtIHN0ICkgPD0gZGVsdGEgKSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblxuXHRcdFx0Ly8gSWYgdGhleSBzY3JvbGxlZCBkb3duIGFuZCBhcmUgcGFzdCB0aGUgbmF2YmFyLCBhZGQgY2xhc3MgLm5hdi11cC5cblx0XHRcdC8vIFRoaXMgaXMgbmVjZXNzYXJ5IHNvIHlvdSBuZXZlciBzZWUgd2hhdCBpcyBcImJlaGluZFwiIHRoZSBuYXZiYXIuXG5cdFx0XHRpZiAoIHN0ID4gbGFzdFNjcm9sbFRvcCAmJiBzdCA+IG5hdmJhckhlaWdodCApIHtcblx0XHRcdFx0Ly8gU2Nyb2xsIERvd25cblx0XHRcdFx0JCggJ2hlYWRlcicgKS5yZW1vdmVDbGFzcyggJ25hdi1kb3duJyApLmFkZENsYXNzKCAnbmF2LXVwJyApO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Ly8gU2Nyb2xsIFVwXG5cdFx0XHRcdGlmICggc3QgKyAkKCB3aW5kb3cgKS5oZWlnaHQoKSA8ICQoIGRvY3VtZW50ICkuaGVpZ2h0KCkgKSB7XG5cdFx0XHRcdFx0JCggJ2hlYWRlcicgKS5yZW1vdmVDbGFzcyggJ25hdi11cCcgKS5hZGRDbGFzcyggJ25hdi1kb3duJyApO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdGxhc3RTY3JvbGxUb3AgPSBzdDtcblx0XHR9XG5cblx0XHR3b3cgPSBuZXcgV09XKFxuXHRcdFx0e1xuXHRcdFx0XHRhbmltYXRlQ2xhc3M6ICdhbmltYXRlZCcsXG5cdFx0XHRcdG9mZnNldDogMTAwXG5cdFx0XHR9XG5cdFx0KTtcblx0XHR3b3cuaW5pdCgpO1xuXG5cdH0gKTtcbn0gKTsiXSwiZmlsZSI6ImFwcC9tYWluLmpzIn0=
