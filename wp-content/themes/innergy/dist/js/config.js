/**
 * Config
 *
 * The RequireJS config for more information about making a config read the documentation
 * http://requirejs.org/docs/api.html#config
 */

requirejs.config( {
	baseUrl: variables.base_url,
	paths: {
		app: '../app',
		components: '../components'
	}
} );
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb25maWcuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb25maWdcbiAqXG4gKiBUaGUgUmVxdWlyZUpTIGNvbmZpZyBmb3IgbW9yZSBpbmZvcm1hdGlvbiBhYm91dCBtYWtpbmcgYSBjb25maWcgcmVhZCB0aGUgZG9jdW1lbnRhdGlvblxuICogaHR0cDovL3JlcXVpcmVqcy5vcmcvZG9jcy9hcGkuaHRtbCNjb25maWdcbiAqL1xuXG5yZXF1aXJlanMuY29uZmlnKCB7XG5cdGJhc2VVcmw6IHZhcmlhYmxlcy5iYXNlX3VybCxcblx0cGF0aHM6IHtcblx0XHRhcHA6ICcuLi9hcHAnLFxuXHRcdGNvbXBvbmVudHM6ICcuLi9jb21wb25lbnRzJ1xuXHR9XG59ICk7Il0sImZpbGUiOiJjb25maWcuanMifQ==
