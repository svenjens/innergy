/**
 * jQuery
 *
 * jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document
 * traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API t
 * hat works across a multitude of browsers. With a combination of versatility and extensibility, jQuery has
 * changed the way that millions of people write JavaScript.
 */

// Use jQuery defined by WordPress
define( 'jquery', [], function() {
	return window.jQuery;
} );
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJ2ZW5kb3IvanF1ZXJ5LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogalF1ZXJ5XG4gKlxuICogalF1ZXJ5IGlzIGEgZmFzdCwgc21hbGwsIGFuZCBmZWF0dXJlLXJpY2ggSmF2YVNjcmlwdCBsaWJyYXJ5LiBJdCBtYWtlcyB0aGluZ3MgbGlrZSBIVE1MIGRvY3VtZW50XG4gKiB0cmF2ZXJzYWwgYW5kIG1hbmlwdWxhdGlvbiwgZXZlbnQgaGFuZGxpbmcsIGFuaW1hdGlvbiwgYW5kIEFqYXggbXVjaCBzaW1wbGVyIHdpdGggYW4gZWFzeS10by11c2UgQVBJIHRcbiAqIGhhdCB3b3JrcyBhY3Jvc3MgYSBtdWx0aXR1ZGUgb2YgYnJvd3NlcnMuIFdpdGggYSBjb21iaW5hdGlvbiBvZiB2ZXJzYXRpbGl0eSBhbmQgZXh0ZW5zaWJpbGl0eSwgalF1ZXJ5IGhhc1xuICogY2hhbmdlZCB0aGUgd2F5IHRoYXQgbWlsbGlvbnMgb2YgcGVvcGxlIHdyaXRlIEphdmFTY3JpcHQuXG4gKi9cblxuLy8gVXNlIGpRdWVyeSBkZWZpbmVkIGJ5IFdvcmRQcmVzc1xuZGVmaW5lKCAnanF1ZXJ5JywgW10sIGZ1bmN0aW9uKCkge1xuXHRyZXR1cm4gd2luZG93LmpRdWVyeTtcbn0gKTsiXSwiZmlsZSI6InZlbmRvci9qcXVlcnkuanMifQ==
