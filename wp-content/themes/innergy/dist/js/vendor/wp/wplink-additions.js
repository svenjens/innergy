/**
 * WP Link additions
 *
 * This script extends the default edit link box of WordPress. It generates a "Make this link a button"
 * checkbox which will render the <a class="button"></a> in the frontend.
 */

(function( $ ) {
	$( document ).ready(
		function() {
			var wpLinkGetAttrs = wpLink.getAttrs;
			var wpLinkRefresh = wpLink.refresh;

			$( '#wp-link-wrap .wp-link-text-field' ).show();
			$( '#wp-link .query-results' ).css( 'top', '210px' );

			$(
				'<div class="link-button" style="margin: 0 0 -15px 3px;">' +
				'<label><span> </span><input type="checkbox" id="wp-link-button" /> ' + objectL10n.make_button + '</label>' +
				'</div>'
			).insertAfter( '#wp-link .link-target' );

			wpLink.getAttrs = function() {
				var attributes = wpLinkGetAttrs.apply( wpLinkGetAttrs );

				attributes.class = $( '#wp-link-button' ).is( ':checked' ) ? 'button' : '';

				return attributes;
			};

			wpLink.refresh = function() {
				if ( wpLink.isMCE() ) {
					wpLinkRefresh.apply( wpLinkRefresh );

					var editor = window.tinymce.get( window.wpActiveEditor );
					var linkNode = editor.dom.getParent( editor.selection.getNode(), 'a[href]' );

					$( '#wp-link-button' ).prop( 'checked', 'button' === editor.dom.getAttrib( linkNode, 'class' ) );
				}
			};
		}
	);
})( jQuery );

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJ2ZW5kb3Ivd3Avd3BsaW5rLWFkZGl0aW9ucy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFdQIExpbmsgYWRkaXRpb25zXG4gKlxuICogVGhpcyBzY3JpcHQgZXh0ZW5kcyB0aGUgZGVmYXVsdCBlZGl0IGxpbmsgYm94IG9mIFdvcmRQcmVzcy4gSXQgZ2VuZXJhdGVzIGEgXCJNYWtlIHRoaXMgbGluayBhIGJ1dHRvblwiXG4gKiBjaGVja2JveCB3aGljaCB3aWxsIHJlbmRlciB0aGUgPGEgY2xhc3M9XCJidXR0b25cIj48L2E+IGluIHRoZSBmcm9udGVuZC5cbiAqL1xuXG4oZnVuY3Rpb24oICQgKSB7XG5cdCQoIGRvY3VtZW50ICkucmVhZHkoXG5cdFx0ZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgd3BMaW5rR2V0QXR0cnMgPSB3cExpbmsuZ2V0QXR0cnM7XG5cdFx0XHR2YXIgd3BMaW5rUmVmcmVzaCA9IHdwTGluay5yZWZyZXNoO1xuXG5cdFx0XHQkKCAnI3dwLWxpbmstd3JhcCAud3AtbGluay10ZXh0LWZpZWxkJyApLnNob3coKTtcblx0XHRcdCQoICcjd3AtbGluayAucXVlcnktcmVzdWx0cycgKS5jc3MoICd0b3AnLCAnMjEwcHgnICk7XG5cblx0XHRcdCQoXG5cdFx0XHRcdCc8ZGl2IGNsYXNzPVwibGluay1idXR0b25cIiBzdHlsZT1cIm1hcmdpbjogMCAwIC0xNXB4IDNweDtcIj4nICtcblx0XHRcdFx0JzxsYWJlbD48c3Bhbj4gPC9zcGFuPjxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBpZD1cIndwLWxpbmstYnV0dG9uXCIgLz4gJyArIG9iamVjdEwxMG4ubWFrZV9idXR0b24gKyAnPC9sYWJlbD4nICtcblx0XHRcdFx0JzwvZGl2Pidcblx0XHRcdCkuaW5zZXJ0QWZ0ZXIoICcjd3AtbGluayAubGluay10YXJnZXQnICk7XG5cblx0XHRcdHdwTGluay5nZXRBdHRycyA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgYXR0cmlidXRlcyA9IHdwTGlua0dldEF0dHJzLmFwcGx5KCB3cExpbmtHZXRBdHRycyApO1xuXG5cdFx0XHRcdGF0dHJpYnV0ZXMuY2xhc3MgPSAkKCAnI3dwLWxpbmstYnV0dG9uJyApLmlzKCAnOmNoZWNrZWQnICkgPyAnYnV0dG9uJyA6ICcnO1xuXG5cdFx0XHRcdHJldHVybiBhdHRyaWJ1dGVzO1xuXHRcdFx0fTtcblxuXHRcdFx0d3BMaW5rLnJlZnJlc2ggPSBmdW5jdGlvbigpIHtcblx0XHRcdFx0aWYgKCB3cExpbmsuaXNNQ0UoKSApIHtcblx0XHRcdFx0XHR3cExpbmtSZWZyZXNoLmFwcGx5KCB3cExpbmtSZWZyZXNoICk7XG5cblx0XHRcdFx0XHR2YXIgZWRpdG9yID0gd2luZG93LnRpbnltY2UuZ2V0KCB3aW5kb3cud3BBY3RpdmVFZGl0b3IgKTtcblx0XHRcdFx0XHR2YXIgbGlua05vZGUgPSBlZGl0b3IuZG9tLmdldFBhcmVudCggZWRpdG9yLnNlbGVjdGlvbi5nZXROb2RlKCksICdhW2hyZWZdJyApO1xuXG5cdFx0XHRcdFx0JCggJyN3cC1saW5rLWJ1dHRvbicgKS5wcm9wKCAnY2hlY2tlZCcsICdidXR0b24nID09PSBlZGl0b3IuZG9tLmdldEF0dHJpYiggbGlua05vZGUsICdjbGFzcycgKSApO1xuXHRcdFx0XHR9XG5cdFx0XHR9O1xuXHRcdH1cblx0KTtcbn0pKCBqUXVlcnkgKTtcbiJdLCJmaWxlIjoidmVuZG9yL3dwL3dwbGluay1hZGRpdGlvbnMuanMifQ==
