/**
 * Testimonial
 *
 * You can require it on the page using <div data-component="content-slider"></div>
 */
define( [ 'jquery', 'slick' ], function( $ ) {
	$( function() {
		$( '[data-component="content-slider"]' ).each( function() {

			$( this ).slick(
				{
					rows: 0,
					slidesToShow: 2,
					slidesToScroll: 1,
					prevArrow: '<button class="content-slider__prev"></button>',
					nextArrow: '<button class="content-slider__next"></button>',
					variableWidth: true
				}
			);
		} );
	} );
} );
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL2NvbnRlbnQtc2xpZGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogVGVzdGltb25pYWxcbiAqXG4gKiBZb3UgY2FuIHJlcXVpcmUgaXQgb24gdGhlIHBhZ2UgdXNpbmcgPGRpdiBkYXRhLWNvbXBvbmVudD1cImNvbnRlbnQtc2xpZGVyXCI+PC9kaXY+XG4gKi9cbmRlZmluZSggWyAnanF1ZXJ5JywgJ3NsaWNrJyBdLCBmdW5jdGlvbiggJCApIHtcblx0JCggZnVuY3Rpb24oKSB7XG5cdFx0JCggJ1tkYXRhLWNvbXBvbmVudD1cImNvbnRlbnQtc2xpZGVyXCJdJyApLmVhY2goIGZ1bmN0aW9uKCkge1xuXG5cdFx0XHQkKCB0aGlzICkuc2xpY2soXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRyb3dzOiAwLFxuXHRcdFx0XHRcdHNsaWRlc1RvU2hvdzogMixcblx0XHRcdFx0XHRzbGlkZXNUb1Njcm9sbDogMSxcblx0XHRcdFx0XHRwcmV2QXJyb3c6ICc8YnV0dG9uIGNsYXNzPVwiY29udGVudC1zbGlkZXJfX3ByZXZcIj48L2J1dHRvbj4nLFxuXHRcdFx0XHRcdG5leHRBcnJvdzogJzxidXR0b24gY2xhc3M9XCJjb250ZW50LXNsaWRlcl9fbmV4dFwiPjwvYnV0dG9uPicsXG5cdFx0XHRcdFx0dmFyaWFibGVXaWR0aDogdHJ1ZVxuXHRcdFx0XHR9XG5cdFx0XHQpO1xuXHRcdH0gKTtcblx0fSApO1xufSApOyJdLCJmaWxlIjoiY29tcG9uZW50cy9jb250ZW50LXNsaWRlci5qcyJ9
