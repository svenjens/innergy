/**
 * Testimonial
 *
 * You can require it on the page using <div data-component="testimonial"></div>
 */
define( [ 'jquery', 'slick' ], function( $ ) {
	$( function() {
		$( '[data-component="testimonial"]' ).each( function() {

			$( this ).slick(
				{
					rows: 0,
					dots: true,
					arrows: false,
					dotsClass: 'testimonial__slick-dots',
					autoplay: true,
					autoplaySpeed: 4000,
				}
			);
		} );
	} );
} );
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL3Rlc3RpbW9uaWFsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogVGVzdGltb25pYWxcbiAqXG4gKiBZb3UgY2FuIHJlcXVpcmUgaXQgb24gdGhlIHBhZ2UgdXNpbmcgPGRpdiBkYXRhLWNvbXBvbmVudD1cInRlc3RpbW9uaWFsXCI+PC9kaXY+XG4gKi9cbmRlZmluZSggWyAnanF1ZXJ5JywgJ3NsaWNrJyBdLCBmdW5jdGlvbiggJCApIHtcblx0JCggZnVuY3Rpb24oKSB7XG5cdFx0JCggJ1tkYXRhLWNvbXBvbmVudD1cInRlc3RpbW9uaWFsXCJdJyApLmVhY2goIGZ1bmN0aW9uKCkge1xuXG5cdFx0XHQkKCB0aGlzICkuc2xpY2soXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRyb3dzOiAwLFxuXHRcdFx0XHRcdGRvdHM6IHRydWUsXG5cdFx0XHRcdFx0YXJyb3dzOiBmYWxzZSxcblx0XHRcdFx0XHRkb3RzQ2xhc3M6ICd0ZXN0aW1vbmlhbF9fc2xpY2stZG90cycsXG5cdFx0XHRcdFx0YXV0b3BsYXk6IHRydWUsXG5cdFx0XHRcdFx0YXV0b3BsYXlTcGVlZDogNDAwMCxcblx0XHRcdFx0fVxuXHRcdFx0KTtcblx0XHR9ICk7XG5cdH0gKTtcbn0gKTsiXSwiZmlsZSI6ImNvbXBvbmVudHMvdGVzdGltb25pYWwuanMifQ==
