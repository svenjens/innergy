/**
 * Popup
 *
 * This component requires jQuery and Magnific popup to function.
 * You can require it on the page using <div data-component="popup"></div>
 */

define( [ 'jquery', 'magnific-popup' ], function( $ ) {
	$( function() {

		$('.popup-modal').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#username',
			modal: true
		});

		$(document).on('click', '.popup-modal-dismiss', function (e) {
			e.preventDefault();
			$.magnificPopup.close();
		});

	} );
} );
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL3BvcHVwLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogUG9wdXBcbiAqXG4gKiBUaGlzIGNvbXBvbmVudCByZXF1aXJlcyBqUXVlcnkgYW5kIE1hZ25pZmljIHBvcHVwIHRvIGZ1bmN0aW9uLlxuICogWW91IGNhbiByZXF1aXJlIGl0IG9uIHRoZSBwYWdlIHVzaW5nIDxkaXYgZGF0YS1jb21wb25lbnQ9XCJwb3B1cFwiPjwvZGl2PlxuICovXG5cbmRlZmluZSggWyAnanF1ZXJ5JywgJ21hZ25pZmljLXBvcHVwJyBdLCBmdW5jdGlvbiggJCApIHtcblx0JCggZnVuY3Rpb24oKSB7XG5cblx0XHQkKCcucG9wdXAtbW9kYWwnKS5tYWduaWZpY1BvcHVwKHtcblx0XHRcdHR5cGU6ICdpbmxpbmUnLFxuXHRcdFx0cHJlbG9hZGVyOiBmYWxzZSxcblx0XHRcdGZvY3VzOiAnI3VzZXJuYW1lJyxcblx0XHRcdG1vZGFsOiB0cnVlXG5cdFx0fSk7XG5cblx0XHQkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLnBvcHVwLW1vZGFsLWRpc21pc3MnLCBmdW5jdGlvbiAoZSkge1xuXHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0JC5tYWduaWZpY1BvcHVwLmNsb3NlKCk7XG5cdFx0fSk7XG5cblx0fSApO1xufSApOyJdLCJmaWxlIjoiY29tcG9uZW50cy9wb3B1cC5qcyJ9
