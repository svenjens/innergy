/**
 * Slider
 *
 * You can require it on the page using <div data-component="slider"></div>
 */
define( [ 'jquery', 'slick' ], function( $ ) {
	$( function() {
		$( '[data-component="slider"]' ).each( function() {

			$( this ).slick(
				{
					rows: 0,
					dots: true,
					arrows: false,
					dotsClass: 'popular__slick-dots',
					infinite: false
				}
			);

			$( this ).on('wheel', (function(e) {
				e.preventDefault();

				if (e.originalEvent.deltaY < 0) {
					$(this).slick('slickNext');
				} else {
					$(this).slick('slickPrev');
				}
			}));

		} );
	} );
} );
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL3NsaWRlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFNsaWRlclxuICpcbiAqIFlvdSBjYW4gcmVxdWlyZSBpdCBvbiB0aGUgcGFnZSB1c2luZyA8ZGl2IGRhdGEtY29tcG9uZW50PVwic2xpZGVyXCI+PC9kaXY+XG4gKi9cbmRlZmluZSggWyAnanF1ZXJ5JywgJ3NsaWNrJyBdLCBmdW5jdGlvbiggJCApIHtcblx0JCggZnVuY3Rpb24oKSB7XG5cdFx0JCggJ1tkYXRhLWNvbXBvbmVudD1cInNsaWRlclwiXScgKS5lYWNoKCBmdW5jdGlvbigpIHtcblxuXHRcdFx0JCggdGhpcyApLnNsaWNrKFxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0cm93czogMCxcblx0XHRcdFx0XHRkb3RzOiB0cnVlLFxuXHRcdFx0XHRcdGFycm93czogZmFsc2UsXG5cdFx0XHRcdFx0ZG90c0NsYXNzOiAncG9wdWxhcl9fc2xpY2stZG90cycsXG5cdFx0XHRcdFx0aW5maW5pdGU6IGZhbHNlXG5cdFx0XHRcdH1cblx0XHRcdCk7XG5cblx0XHRcdCQoIHRoaXMgKS5vbignd2hlZWwnLCAoZnVuY3Rpb24oZSkge1xuXHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cblx0XHRcdFx0aWYgKGUub3JpZ2luYWxFdmVudC5kZWx0YVkgPCAwKSB7XG5cdFx0XHRcdFx0JCh0aGlzKS5zbGljaygnc2xpY2tOZXh0Jyk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0JCh0aGlzKS5zbGljaygnc2xpY2tQcmV2Jyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pKTtcblxuXHRcdH0gKTtcblx0fSApO1xufSApOyJdLCJmaWxlIjoiY29tcG9uZW50cy9zbGlkZXIuanMifQ==
