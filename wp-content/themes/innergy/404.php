<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

<?php if ( ( $post = get_404() ) && ! empty( $post ) ) : ?>
	<?php setup_postdata( $post ); ?>

	<main>
		<div class="content centered"><?php the_content(); ?></div>
	</main>

	<?php wp_reset_postdata(); ?>
<?php else : ?>
	<main>
		<div class="content centered">
			<h1><?php _t( 'Page not Found' ); ?></h1>
		</div>
	</main>
<?php endif; ?>

<?php get_footer(); ?>
