<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the page wrapper div.
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes() ?>>
<head>
	<?php wp_head() ?>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />
</head>

<body <?php body_class() ?>>
<div class="wrapper wrapper--page <?php the_title(); ?>">
	<header class="header centered centered--large">
		<a class="header__logo" href="<?php echo esc_url( home_url() ); ?>">
			<?php if ( ( $logo = get_field( 'header_logo', 'option' ) ) && ! empty( $logo ) ): ?>
				<img class="header__logo-main" src="<?php echo $logo['url']; ?>" alt="<?php bloginfo( 'name' ); ?>">
			<?php else: ?>
				<img class="header__logo-main" src="<?php echo get_template_directory_uri() ?>/dist/img/logo.svg"
				     alt="<?php bloginfo( 'name' ); ?>">
			<?php endif;

			if ( ( $logo_white = get_field( 'logo_white', 'option' ) ) && ! empty( $logo_white ) ): ?>
				<img class="header__logo-white" src="<?php echo $logo_white['url']; ?>"
				     alt="<?php bloginfo( 'name' ); ?>">
			<?php else: ?>
				<img class="header__logo-white" src="<?php echo get_template_directory_uri() ?>/dist/img/logo-white.svg"
				     alt="<?php bloginfo( 'name' ); ?>">
			<?php endif; ?>
		</a>

		<div class="header__main">

			<a href="#" class="header__menu-toggle js-menu-toggle">
				<span></span>
			</a>

			<div class="header__main-menu">
				<?php
				wp_nav_menu( [
					'theme_location' => 'header_menu',
					'container'      => '',
				] );
				?>
				<div class="header__main-lang">
					<?php do_action( 'wpml_add_language_selector' ); ?>
				</div>
			</div>

			<?php if ( ( $button = get_field( 'header_button', 'option' ) ) && ! empty( $button ) ): ?>
				<a class="button button--calendor header__button" href="<?php echo $button['url']; ?>"
				   target="<?php echo $button['target']; ?>">
					<?php echo $button['title']; ?>
				</a>
			<?php endif; ?>
		</div>

		<div class="header__mobile js-mobile">

			<div class="header__mobile-lang">
				<?php do_action( 'wpml_add_language_selector' ); ?>
			</div>

			<div class="header__mobile-wrap">
				<?php wp_nav_menu( [
					'theme_location' => 'header_menu',
					'container'      => '',
				] );
				?>

			</div>
			<?php

			$linkedin  = get_field( 'linkedin_url', 'option' );
			$facebook  = get_field( 'facebook_url', 'option' );
			$instagram = get_field( 'instagram_url', 'option' );

			if ( ! empty( $linkedin ) || ! empty( $facebook ) || ! empty( $instagram ) ):?>
				<ul class="header__social">
					<?php if ( ! empty( $instagram ) ): ?>
						<li><a href="<?php echo $instagram; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<?php endif;

					if ( ! empty( $facebook ) ): ?>
						<li><a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<?php endif;

					if ( ! empty( $linkedin ) ): ?>
						<li><a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					<?php endif; ?>
				</ul>
			<?php endif; ?>
		</div>

	</header>
