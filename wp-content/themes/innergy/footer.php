<?php
/**
 * The template for displaying the footer
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */
$footer_bg  = get_field( 'footer_bg', 'option' );
$footer_img = get_field( 'footer_image', 'option' );
?>
<footer class="footer">

	<?php if ( ! empty( $footer_bg ) || ! empty( $footer_img ) ): ?>
		<div class="footer__image wow fadeInUp" data-wow-delay="0.4s"
		     style="background-image: url('<?php echo wp_get_attachment_image_url( $footer_bg['id'], 'footer-bg' ); ?>')">
			<?php echo wp_get_attachment_image( $footer_img['id'], 'footer-image' ); ?>
		</div>
	<?php endif; ?>

	<div class="footer__menu centered">
		<?php
		wp_nav_menu( [
			'theme_location' => 'footer_menu',
			'container'      => '',
		] );

		$linkedin  = get_field( 'linkedin_url', 'option' );
		$facebook  = get_field( 'facebook_url', 'option' );
		$instagram = get_field( 'instagram_url', 'option' );

		if ( ! empty( $linkedin ) || ! empty( $facebook ) || ! empty( $instagram ) ):?>
			<ul class="footer__social">
				<?php if ( ! empty( $instagram ) ): ?>
					<li><a href="<?php echo $instagram; ?>" target="_blank"><i class="fa fa-instagram"
					                                                           aria-hidden="true"></i></a></li>
				<?php endif;

				if ( ! empty( $facebook ) ): ?>
					<li><a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook"
					                                                          aria-hidden="true"></i></a></li>
				<?php endif;

				if ( ! empty( $linkedin ) ): ?>
					<li><a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin"
					                                                          aria-hidden="true"></i></a></li>
				<?php endif; ?>
			</ul>
		<?php endif; ?>
	</div>

	<?php
	$content = get_field( 'footer_content', 'option' );
	$button  = get_field( 'footer_button', 'option' );
	if ( ! empty( $content ) || ! empty( $button ) ): ?>
		<div class="footer__bottom centered">
			<?php echo $content;

			if ( ! empty( $button ) ):?>
				<a class="button button--white footer__button" href="<?php echo $button['url']; ?>"
				   target="<?php echo $button['target']; ?>">
					<?php echo $button['title']; ?>
				</a>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</footer>
</div>

<?php wp_footer(); ?>

</body>

</html>
