<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link       https://codex.wordpress.org/Template_Hierarchy
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

	<main role="main">
		<div class="treatments centered">

			<div class="treatments__content">
				<?php echo apply_filters( 'the_content', get_post_field( 'post_content', get_archive()->ID ) ); ?>
			</div>

			<?php if ( have_posts() ) : ?>
				<div class="treatments__list">
					<ul>
						<?php while ( have_posts() ): the_post(); ?>
							<li><?php get_template_part( 'template-parts/loop', 'treatment' ); ?></li>
						<?php endwhile; ?>
					</ul>
				</div>
			<?php endif; ?>

		</div>
		<?php get_template_part( 'template-parts/content', 'flexible' ); ?>
	</main>

<?php get_footer();
