<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link       http://codex.wordpress.org/Template_Hierarchy
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

	<main role="main">
		<?php
		get_template_part( 'template-parts/content', 'blog-content' );
		get_template_part( 'template-parts/content', 'blog' );
		?>
	</main>

<?php get_footer();
