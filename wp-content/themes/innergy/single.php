<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

	<main role="main">
		<?php
		get_template_part( 'template-parts/content', 'single' );
		get_template_part( 'template-parts/content', 'related' );
		?>
	</main>

<?php get_footer();
