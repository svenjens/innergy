/**
 * Main
 *
 * The main file, page logic which is needed on all pages will be included here.
 * Please note for specific components like a slider create a separated components/<component_name>.js
 * The components will be loaded through the component loader and are not needed on every page!
 */

define( ['jquery', 'fastclick', 'mmenu', 'wow'], function( $, fast_click, wow ) {
	$( function() {
		fast_click.attach( document.body );

		$( '#nav' ).mmenu(
			{
				scrollBugFix: true,
				offCanvas: {
					position: 'right'
				},
				navbar: {
					title: ''
				}
			}, {
				clone: true
			}
		);



		$( 'a[href="#top"]' ).on(
			'click', function( event ) {
				event.preventDefault();

				$( 'html, body' ).animate( { scrollTop: 0 } );
			}
		);

		$( '.js-menu-toggle' ).click( function() {
			$( this ).toggleClass( 'is-active' );
			$( '.js-mobile' ).slideToggle();
			$( '.header__logo-white' ).toggleClass( 'header__active-white' );
			$( '.header__logo-main' ).toggleClass( 'header__active-main' );
		} );

		if ( $( window ).width() < 767 ) {
			$( '.js-toggle-mobile' ).click( function() {
				$( this ).next().slideToggle();
				$( this ).toggleClass( 'is-active' );
				$( this ).parent().siblings().find( 'p' ).slideUp();
				$( this ).parent().siblings().find( 'h6' ).removeClass( 'is-active' );
			} );
		}

		$( window ).scroll( function() {
			var scroll = $( window ).scrollTop();

			if ( scroll >= 50 ) {
				$( '.header' ).addClass( 'fixed' );
			} else {
				$( '.header' ).removeClass( 'fixed' );
			}
		} );

		$( '#play-video' ).on( 'click', function( ev ) {
			$( this ).hide();
			$( '#video' )[ 0 ].src += '&autoplay=&mute=1';
			ev.preventDefault();
		} );

		if($(window).width() > 767)
		{
			$(window).scroll(function(e){
				parallaxScroll();
			});

			function parallaxScroll(){
				var scrolled = $(window).scrollTop();
				$('#parallax-bg-1').css('top',(0-(scrolled*.1))+'px');
				$('#parallax-bg-2').css('top',(0-(scrolled*.05))+'px');
			}
		}


		// Hide Header on on scroll down
		var didScroll;
		var lastScrollTop = 0;
		var delta = 5;
		var navbarHeight = $( '.header' ).outerHeight();

		$( window ).scroll( function( event ) {
			didScroll = true;
		} );

		setInterval( function() {
			if ( didScroll ) {
				hasScrolled();
				didScroll = false;
			}
		}, 250 );

		function hasScrolled() {
			var st = $( this ).scrollTop();

			// Make sure they scroll more than delta
			if ( Math.abs( lastScrollTop - st ) <= delta ) {
				return;
			}

			// If they scrolled down and are past the navbar, add class .nav-up.
			// This is necessary so you never see what is "behind" the navbar.
			if ( st > lastScrollTop && st > navbarHeight ) {
				// Scroll Down
				$( 'header' ).removeClass( 'nav-down' ).addClass( 'nav-up' );
			} else {
				// Scroll Up
				if ( st + $( window ).height() < $( document ).height() ) {
					$( 'header' ).removeClass( 'nav-up' ).addClass( 'nav-down' );
				}
			}

			lastScrollTop = st;
		}

		wow = new WOW(
			{
				animateClass: 'animated',
				offset: 100
			}
		);
		wow.init();

	} );
} );