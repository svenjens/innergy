/**
 * Magnific popup
 *
 * Magnific Popup is a responsive lightbox & dialog script with focus on performance and providing best
 * experience for users with any device.
 */

/* jshint ignore:start */
// =require ../../../../../../node_modules/magnific-popup/dist/jquery.magnific-popup.js
/* jshint ignore:end */
