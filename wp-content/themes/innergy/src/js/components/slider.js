/**
 * Slider
 *
 * You can require it on the page using <div data-component="slider"></div>
 */
define( [ 'jquery', 'slick' ], function( $ ) {
	$( function() {
		$( '[data-component="slider"]' ).each( function() {

			$( this ).slick(
				{
					rows: 0,
					dots: true,
					arrows: false,
					dotsClass: 'popular__slick-dots',
					infinite: false
				}
			);

			$( this ).on('wheel', (function(e) {
				e.preventDefault();

				if (e.originalEvent.deltaY < 0) {
					$(this).slick('slickNext');
				} else {
					$(this).slick('slickPrev');
				}
			}));

		} );
	} );
} );