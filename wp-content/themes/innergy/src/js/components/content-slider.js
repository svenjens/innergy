/**
 * Testimonial
 *
 * You can require it on the page using <div data-component="content-slider"></div>
 */
define( [ 'jquery', 'slick' ], function( $ ) {
	$( function() {
		$( '[data-component="content-slider"]' ).each( function() {

			$( this ).slick(
				{
					rows: 0,
					slidesToShow: 2,
					slidesToScroll: 1,
					prevArrow: '<button class="content-slider__prev"></button>',
					nextArrow: '<button class="content-slider__next"></button>',
					variableWidth: true
				}
			);
		} );
	} );
} );