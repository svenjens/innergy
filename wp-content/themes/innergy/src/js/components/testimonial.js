/**
 * Testimonial
 *
 * You can require it on the page using <div data-component="testimonial"></div>
 */
define( [ 'jquery', 'slick' ], function( $ ) {
	$( function() {
		$( '[data-component="testimonial"]' ).each( function() {

			$( this ).slick(
				{
					rows: 0,
					dots: true,
					arrows: false,
					dotsClass: 'testimonial__slick-dots',
					autoplay: true,
					autoplaySpeed: 4000,
				}
			);
		} );
	} );
} );