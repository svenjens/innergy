/**
 * Popup
 *
 * This component requires jQuery and Magnific popup to function.
 * You can require it on the page using <div data-component="popup"></div>
 */

define( [ 'jquery', 'magnific-popup' ], function( $ ) {
	$( function() {

		$('.popup-modal').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#username',
			modal: true
		});

		$(document).on('click', '.popup-modal-dismiss', function (e) {
			e.preventDefault();
			$.magnificPopup.close();
		});

	} );
} );