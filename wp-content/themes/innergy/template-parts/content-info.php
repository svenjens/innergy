<?php
/**
 * The template part for displaying info content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$content = get_sub_field( 'info_content' );
$image   = get_sub_field( 'info_image' );
if ( ! empty( $image ) || ! empty( $content ) ):?>
	<div class="info centered centered--small">
		<div class="info__holder">
			<?php if ( ! empty( $image ) ): ?>
				<figure id="parallax-bg-2">
					<?php echo wp_get_attachment_image( $image['id'], 'info' ); ?>
				</figure>
			<?php endif;

			if ( ! empty( $content ) ): ?>
				<div class="info__content">
					<?php echo $content; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>