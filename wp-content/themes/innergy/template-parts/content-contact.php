<?php
/**
 * The template part for displaying about content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$content = get_sub_field( 'content_d' );
$tel     = get_sub_field( 'telephone_d' );
$mail    = get_sub_field( 'maid_id' );
if ( ! empty( $content ) || ! empty( $tel ) || ! empty( $mail ) ): ?>
	<div class="contact centered centered--small">
		<?php if ( ! empty( $content ) ) : ?>
			<div class="contact__content">
				<?php echo $content; ?>
			</div>
		<?php endif;

		if ( ! empty( $tel ) || ! empty( $mail ) ) : ?>
		<div class="contact__buttons">
			<?php if ( ! empty( $tel )) : ?>
			<a class="button button--blue button--blue-border" href="tel:<?php echo $tel; ?>">
				<?php echo $tel; ?>
			</a>
			<?php endif;
			if ( ! empty( $mail )) : ?>
			<a class="button button--blue" href="mailto:<?php echo $mail; ?>">
				<?php echo $mail; ?>
			</a>
			<?php endif; ?>
		</div>
		<?php endif; ?>
	</div>
<?php endif; ?>