<?php
/**
 * The template part for displaying content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
?>

<?php the_content(); ?>