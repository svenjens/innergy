<?php
/**
 * The template used for displaying flexible content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      4.5.1
 * @version    4.5.1
 */
?>
<a href="<?php the_permalink(); ?>" class="card-post">
	<?php the_post_thumbnail( 'post_image' ); ?>

	<div class="card-post__date">
		<?php $post_date = get_the_date( 'd.m.Y' );
		echo $post_date; ?>
	</div>

	<h5><?php the_title(); ?></h5>
</a>
