<?php
/**
 * The template part for displaying about content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$content = get_sub_field( 'content_re' );
$image   = get_sub_field( 'image_re' );
$contact = get_sub_field( 'contact_info_re' );
$buttons = get_sub_field( 'button_re' );
if ( ! empty( $content ) || ! empty( $image ) || ! empty( $contact ) || ! empty( $buttons ) ) : ?>
	<div class="cont-img centered">
		<?php if ( ! empty( $content ) || ! empty( $image ) ) : ?>
			<div class="cont-img__holder">
				<?php if ( ! empty( $content ) ) : ?>
					<div class="cont-img__content">
						<?php echo $content; ?>
					</div>
				<?php endif;

				if ( ! empty( $image ) ) : ?>
					<div class="cont-img__image">
						<img src="<?php echo wp_get_attachment_image_url( $image['id'], 'image-one' ); ?>"
						     alt=""/>
					</div>
				<?php endif; ?>
			</div>
		<?php endif;

		if ( ! empty( $contact ) || ! empty( $buttons ) ) : ?>
			<div class="cont-img__contact">
				<?php if ( ! empty( $contact ) ) : ?>
					<div class="cont-img__text">
						<?php echo $contact; ?>
					</div>
				<?php endif;

				if ( get_sub_field( 'button_re' ) ): ?>
					<div class="cont-img__buttons">
						<?php while ( has_sub_field( 'button_re' ) ):
							$btn_links = get_sub_field( 'buttons_link' ); ?>
							<a href="<?php echo $btn_links['url']; ?>" target="<?php echo $btn_links['target']; ?>"
							   class="button">
								<?php echo $btn_links['title']; ?>
							</a>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>

			</div>
		<?php endif; ?>
	</div>
<?php endif; ?>
