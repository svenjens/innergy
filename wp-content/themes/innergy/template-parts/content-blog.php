<?php
/**
 * The template part for displaying about content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
?>
<div class="content">
	<?php if ( have_posts() ) : ?>
		<div class="content__blog centered--large">
			<?php while ( have_posts() ) {
				the_post();

				/**
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/loop', 'post' );
			} ?>
		</div>
	<?php else: ?>
		<div class="content__text">
			<?php get_template_part( 'template-parts/content', 'none' ); ?>
		</div>
	<?php endif; ?>
</div>
