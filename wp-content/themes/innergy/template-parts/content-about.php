<?php
/**
 * The template part for displaying about content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$image            = get_sub_field( 'about_image' );
$content          = get_sub_field( 'about_content' );
$person_image     = get_sub_field( 'about_person_image' );
$name             = get_sub_field( 'about_person_name' );
$role             = get_sub_field( 'about_person_role' );
$addition_content = get_sub_field( 'about_additional_content' );
$button           = get_sub_field( 'about_button' );
if ( ! empty( $image ) || ! empty( $content ) || ! empty( $person_image ) || ! empty( $name ) || ! empty( $role ) || ! empty( $addition_content ) || ! empty( $button ) ): ?>
	<div class="about centered centered--small">

		<div class="about__top">
			<?php if ( ! empty( $image ) ): ?>
				<figure id="parallax-bg-1">
					<?php echo wp_get_attachment_image( $image['id'], 'about-image' ); ?>
				</figure>
			<?php endif;

			if ( ! empty( $content ) ) : ?>
				<div class="about__brown-box">
					<?php echo $content; ?>
				</div>
			<?php endif; ?>
		</div>

		<?php if ( ! empty( $person_image ) || ! empty( $name ) || ! empty( $role ) || ! empty( $addition_content ) || ! empty( $button ) ): ?>
			<div class="about__detail">

				<div class="about__info-image">

					<?php if ( ! empty( $person_image ) ): ?>
						<figure><?php echo wp_get_attachment_image( $person_image['id'], 'about-person' ); ?></figure>
					<?php endif; ?>

					<div class="about__info">
						<?php if ( ! empty( $name ) ): ?>
							<h4><?php echo $name; ?></h4>
						<?php endif;

						if ( ! empty( $role ) ):?>
							<span><?php echo $role; ?></span>
						<?php endif; ?>
					</div>

				</div>

				<?php echo $addition_content; ?>

				<?php if ( ! empty( $button ) ): ?>
					<a class="button button--link" href="<?php echo $button['url']; ?>"
					   target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
<?php endif; ?>