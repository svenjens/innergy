<?php
/**
 * The template part for displaying cycle content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$image            = get_sub_field( 'cycle_image' );
$section_title    = get_sub_field( 'cycle_section_title' );
$content          = get_sub_field( 'cycle_content' );
$addition_content = get_sub_field( 'cycle_additional_content' );

if ( ( have_rows( 'cycle_steps' ) ) || ! empty( $image ) || ! empty( $title ) || ! empty( $content ) || ! empty( $addition_content ) ):?>
	<div class="cycle container" data-component="popup">

		<div class="cycle__heading centered"
		     style="background-image: url('<?php echo $image['sizes']['testimonial']; ?>')">
			<?php if ( ! empty( $section_title ) ): ?>
				<h3><?php echo $section_title; ?></h3>
			<?php endif; ?>
		</div>

		<div class="cycle__content centered">
			<?php echo $content; ?>
		</div>

		<?php if ( have_rows( 'cycle_steps' ) ): ?>
			<div class="cycle__list centered">
				<ul>
					<?php $i = 1;
					while ( have_rows( 'cycle_steps' ) ): the_row();
						$image = get_sub_field( 'cycle_step_image' );
						$title = get_sub_field( 'cycle_step_title' );
						if ( ! empty( $image ) || ! empty( $title ) ):?>
							<li>
								<a href="#cycle<?php echo $i; ?>" class="popup-modal">
									<?php if ( ! empty( $image ) ): ?>
										<figure><?php echo wp_get_attachment_image( $image['id'], 'cycle-step' ); ?></figure>
									<?php endif;

									if ( ! empty( $title ) ):?>
										<h6><?php echo $title; ?></h6>
									<?php endif; ?>
								</a>
							</li>
						<?php endif;
						$i ++;
					endwhile; ?>
				</ul>
			</div>
		<?php endif;

		if ( ! empty( $addition_content ) ):?>
			<div class="cycle__additional centered">
				<?php echo $addition_content; ?>
			</div>
		<?php endif;

		$i = 1;

		while ( have_rows( 'cycle_steps' ) ): the_row();

			$content = get_sub_field( 'cycle_step_content' );
			$title   = get_sub_field( 'cycle_step_title' );

			if ( ! empty( $content ) || ! empty( $title ) ):?>
				<div class="mfp-hide white-popup-block" id="cycle<?php echo $i; ?>">
					<a class="white-popup-block__dismiss popup-modal-dismiss" href="#">Dismiss</a>
					<h2><?php echo $title; ?></h2>
					<div class="white-popup-block__content">
						<?php echo $content; ?>
					</div>
				</div>
			<?php endif;
			$i ++;
		endwhile; ?>

	</div>
<?php endif; ?>