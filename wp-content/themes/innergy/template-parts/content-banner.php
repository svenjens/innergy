<?php
/**
 * The template part for displaying action content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$featured_img_url = get_the_post_thumbnail_url( get_the_ID(), 'full' );
$title            = get_the_title();
if ( ! empty( $featured_img_url ) ) : ?>
	<section class="banner" style="background-image: url(<?php echo $featured_img_url; ?>);">
		<h1 class="banner__title"><?php echo $title; ?></h1>
	</section>
<?php endif; ?>