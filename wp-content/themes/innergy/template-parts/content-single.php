<?php
/**
 * The template part for displaying about content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
?>
<div class="content">
	<?php if ( have_posts() ) : ?>
		<div class="content__single centered">
			<?php while ( have_posts() ) {
				the_post();
				if ( has_post_thumbnail() ) : ?>
					<div class="content__image">
						<?php the_post_thumbnail( 'post-single' ); ?>
					</div>
				<?php endif;

				if ( '' !== get_post()->post_content ) : ?>
					<div class="content__content-single">
						<h5><?php the_title(); ?></h5>
						<?php the_content(); ?>
					</div>
				<?php endif;
			} ?>
		</div>
	<?php endif; ?>
</div>
