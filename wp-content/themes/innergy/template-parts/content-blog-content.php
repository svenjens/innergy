<?php
/**
 * The template part for displaying about content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$content = get_field('content_editor', get_option( 'page_for_posts'));
?>
<div class="content__info centered">
	<div class="content__blog-top">
		<?php echo $content; ?>
	</div>

	<?php

	$linkedin  = get_field( 'linkedin_url', 'option' );
	$facebook  = get_field( 'facebook_url', 'option' );
	$instagram = get_field( 'instagram_url', 'option' );

	if ( ! empty( $linkedin ) || ! empty( $facebook ) || ! empty( $instagram ) ):?>
		<ul class="content__social">
			<?php if ( ! empty( $linkedin ) ): ?>
				<li><a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				</li>
			<?php endif;

			if ( ! empty( $facebook ) ): ?>
				<li><a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				</li>
			<?php endif;

			if ( ! empty( $instagram ) ): ?>
				<li><a href="<?php echo $instagram; ?>" target="_blank"><i class="fa fa-instagram"
				                                                           aria-hidden="true"></i></a></li>
			<?php endif; ?>
		</ul>
	<?php endif; ?>
</div>
