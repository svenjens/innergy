<?php
/**
 * The template part for displaying action content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$title   = get_sub_field( 'action_title' );
$content = get_sub_field( 'action_content' );
$button  = get_sub_field( 'action_button' );
if ( ! empty( $title ) || ! empty( $content ) || ! empty( $button ) ): ?>
	<div class="action centered">

		<div class="action__holder">
			<?php if ( ! empty( $title ) || ! empty( $content ) ): ?>
				<div class="action__content">
					<?php if ( ! empty( $title ) ): ?>
						<h3><?php echo $title; ?></h3>
					<?php endif; ?>
					<p><?php echo $content; ?></p>
				</div>
			<?php endif;

			if ( ! empty( $button ) ):?>
				<a href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"
				   class="button button--calendor button--blue">
					<?php echo $button['title']; ?>
				</a>
			<?php endif; ?>
		</div>

	</div>
<?php endif; ?>