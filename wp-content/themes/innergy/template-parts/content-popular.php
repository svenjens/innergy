<?php
/**
 * The template part for displaying treatment content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$content           = get_sub_field( 'treatment_content' );
$select_treatments = get_sub_field( 'select_treatment' );
$count             = count( $select_treatments );
if ( ! empty( $content ) || ! empty( $select_treatments ) ):?>
	<div class="popular">
		<div class="row centered centered--large popular__top">
			<div class="col-md-8 popular__left">
				<?php echo $content; ?>
			</div>
			<div class="col-md-4 popular__right">
				<a class="popular__link"
				   href="<?php get_post_type_archive_link( 'treatment' ); ?>"><?php _t( 'Bekijk hier al onze behandelmogelijkheden' ); ?></a>
			</div>
		</div>
		<?php
		$args    = [
			'post_type' => 'treatment',
			'post__in'  => $select_treatments,
			'orderby'   => 'post__in',
		];
		$popular = new WP_Query( $args );
		if ( $popular->have_posts() ):?>
			<div class="popular__slider" data-component="slider">
				<?php $i = 1;
				while ( $popular->have_posts() ): $popular->the_post(); ?>
					<div class="popular__slide"
					     style="background-image: url('<?php the_post_thumbnail_url( 'treatment-slide' ); ?>');">
						<a href="<?php the_permalink(); ?>" class="popular__inner-slide centered centered--large">
							<div class="popular__center">
								<h4><?php _t( 'Behandelmogelijkheden ' );
									echo str_pad( $i, 2, "0", STR_PAD_LEFT ); ?>
									/<?php echo str_pad( $count, 2, "0", STR_PAD_LEFT ); ?></h4>

								<h2><?php the_title(); ?></h2>
								<?php the_excerpt(); ?>
							</div>
						</a>
					</div>
					<?php $i ++; endwhile;
				wp_reset_postdata(); ?>
			</div>
		<?php endif; ?>
	</div>
<?php endif; ?>