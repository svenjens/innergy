<a href="<?php the_permalink(); ?>">
	<?php if ( has_post_thumbnail() ): ?>
		<figure>
			<?php the_post_thumbnail( 'treatment-thumb' ); ?>
			<div class="treatments__hover">
				<h2><?php the_title(); ?></h2>
				<span>Less Meer</span>
			</div>
		</figure>
	<?php endif;

	the_excerpt(); ?>

	<h4><?php the_title(); ?></h4>
</a>