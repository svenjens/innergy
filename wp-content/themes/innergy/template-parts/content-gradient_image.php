<?php
/**
 * The template part for displaying about content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$image = get_sub_field( 'image_g' );
if(!empty($image)) : ?>
<div class="gradient-image" style="background-image: url(<?php echo $image; ?>);"></div>
<?php endif; ?>
