<?php
/**
 * The template part for displaying education & certification content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$title   = get_field( 'ec_title', 'option' );
$content = get_field( 'ec_content', 'option' );
if ( ( have_rows( 'ec_list', 'option' ) ) || ! empty( $title ) || ! empty( $content ) ):?>
	<div class="certification centered">
		<?php if ( ! empty( $title ) || ! empty( $content ) ): ?>
			<div class="row certification__top">

				<?php if ( $title ): ?>
					<div class="col-md-6 certification__heading">
						<h3>
							<?php echo $title; ?>
						</h3>
					</div>
				<?php endif;

				if ( ! empty( $content ) ):?>
					<div class="col-md-6 certification__content">
						<?php echo $content; ?>
					</div>
				<?php endif; ?>

			</div>
		<?php endif;
		if ( have_rows( 'ec_list', 'option' ) ): ?>
			<ul class="certification__list">
				<?php while ( have_rows( 'ec_list', 'option' ) ): the_row();
					$title   = get_sub_field( 'ec_list_title' );
					$content = get_sub_field( 'ec_list_content' );
					if ( ! empty( $title ) || ! empty( $content ) ):?>
						<li>
							<?php if ( ! empty( $title ) ): ?>
								<h6 class="js-toggle-mobile"><?php echo $title; ?></h6>
							<?php endif;

							echo $content; ?>
						</li>
					<?php endif;
				endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>
<?php endif; ?>