<?php
/**
 * The template part for displaying action content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

?>
<div class="content">
	<?php if ( '' !== get_post()->post_content ) : ?>
		<div class="content__single-treatment centered--exsmall">
			<?php the_content(); ?>
		</div>
	<?php endif; ?>

	<?php
	$title = get_field( 'title_df' );
	$video = get_field( 'embeded_code' );
	$image = get_field( 'image_df' );
	if ( ! empty( $video ) ) : ?>
		<div class="content__video centered--exsmall">
			<?php if ( ! empty( $title ) ) : ?>
				<p><?php echo $title; ?></p>
			<?php endif; ?>
			<div class="content__embed">
				<?php echo $video;
				if(!empty($image)) : ?>
				<a class="content__image-video" href="#" id="play-video">
					<img src="<?php echo wp_get_attachment_image_url( $image['id'], 'video-image' ); ?>"
					     alt=""/>
				</a>
				<?php endif; ?>
			</div>
		</div>
	<?php endif;

	$temp = get_the_ID();

	$args  = [
		'post_type'      => 'treatment',
		'posts_per_page' => - 1,
	];
	$query = new WP_Query( $args ); ?>
	<?php if ( $query->have_posts() ) : ?>
		<div class="content__showall">
			<div class="content__heading centered">
				<h2><?php _t( 'Bekijk ook onze <br />andere behandelingen' ); ?></h2>
			</div>
			<div class="content__list-button centered">
				<?php while ( $query->have_posts() ) : $query->the_post();
					$postid = get_the_ID(); ?>
					<a class="button button--blue content__repeat-list <?php if ( $temp == $postid ) {
						echo "button--active-blue";
					} ?>" href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
				<?php endwhile; ?>
			</div>
		</div>
		<?php wp_reset_postdata(); ?>
	<?php endif; ?>

</div>
