<?php
/**
 * The template part for displaying action content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

if ( function_exists( 'yoast_breadcrumb' ) ) { ?>
	<div class="breadcrumb centered--exsmall">
		<?php yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' ); ?>
	</div>
<?php } ?>