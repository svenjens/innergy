<?php
/**
 * The template used for displaying page content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      4.5.1
 * @version    4.5.1
 */

// Load the ACF content
if ( have_rows( 'content' ) ) {
	while ( have_rows( 'content' ) ) {
		the_row();

		get_template_part( 'template-parts/content', get_row_layout() );
	}
} else { if( '' !== get_post()->post_content ) { ?>
	<div class="content centered centered--exsmall">
		<?php the_content(); ?>
	</div>
	<?php }
 }
