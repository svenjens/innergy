<?php
/**
 * The template part for displaying flexible content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
if ( is_post_type_archive() ) {
	$id = get_archive()->ID;
} else {
	$id = get_the_ID();
}

if ( have_rows( 'content_sections', $id ) ):

	while ( have_rows( 'content_sections', $id ) ): the_row();

		if ( get_row_layout() == 'treatment_section' ):

			get_template_part( 'template-parts/content', 'popular' );

		elseif ( get_row_layout() == 'about_section' ):

			get_template_part( 'template-parts/content', 'about' );

		elseif ( get_row_layout() == 'testimonial_section' ):

			get_template_part( 'template-parts/content', 'testimonial' );

		elseif ( get_row_layout() == 'content_slider_section' ):

			get_template_part( 'template-parts/content', 'slider' );

		elseif ( get_row_layout() == 'action_section' ):

			get_template_part( 'template-parts/content', 'action' );

		elseif ( get_row_layout() == 'educations_&_certification_section' ):

			get_template_part( 'template-parts/content', 'certification' );

		elseif ( get_row_layout() == 'info_section' ):

			get_template_part( 'template-parts/content', 'info' );

		elseif ( get_row_layout() == 'cycle_section' ):

			get_template_part( 'template-parts/content', 'cycle' );

		elseif ( get_row_layout() == 'contact_section' ):

			get_template_part( 'template-parts/content', 'contact' );

		elseif ( get_row_layout() == 'gradient_image' ):

			get_template_part( 'template-parts/content', 'gradient_image' );

		elseif ( get_row_layout() == 'content_image_and_contact_info' ):

			get_template_part( 'template-parts/content', 'cont_img_contact' );

		elseif ( get_row_layout() == 'form_shortcode_and_content' ):

			get_template_part( 'template-parts/content', 'form-content' );

		endif;

	endwhile;

endif; ?>