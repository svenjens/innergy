<?php
/**
 * The template part for displaying about content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
?>
<div class="content">

	<?php
	$orig_post = $post;
	global $post;
	$tags = wp_get_post_tags( $post->ID );

	if ( $tags ) {
		$tag_ids = [];
		foreach ( $tags as $individual_tag ) {
			$tag_ids[] = $individual_tag->term_id;
		}
		$args = [
			'tag__in'          => $tag_ids,
			'post__not_in'     => [ $post->ID ],
			'posts_per_page'   => 9
		];

		$my_query = new wp_query( $args ); ?>
		<div class="content__heading centered--large">
			<h2><?php _t( 'Bekijk ook onze andere <br /> nieuws items en laat je inspireren.' ); ?></h2>
		</div>
		<div class="content__blog centered--large">


			<?php while ( $my_query->have_posts() ) {
				$my_query->the_post();

				get_template_part( 'template-parts/loop', 'post' );
			} ?>
		</div>
	<?php }
	$post = $orig_post;
	wp_reset_query();
	?>

</div>
