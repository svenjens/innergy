<?php

/**
 * The template part for displaying testimonial content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$section_title = get_sub_field( 'testimonial_section_title' );
$image         = get_sub_field( 'testimonial_image' );
if ( ( have_rows( 'testimonials' ) ) || ! empty( $section_title ) || ! empty( $image ) ): ?>
	<div class="testimonial">

		<?php if ( ! empty( $image ) ) : ?>
			<div class="testimonial__heading centered"
			     style="background-image: url('<?php echo $image['sizes']['testimonial']; ?>')">
				<?php if ( ! empty( $section_title ) ): ?>
					<h2><?php echo $section_title; ?></h2>
				<?php endif; ?>
			</div>
		<?php else : ?>
			<?php if ( ! empty( $section_title ) ): ?>
				<h2 class="centered"><?php echo $section_title; ?></h2>
			<?php endif; ?>
		<?php endif; ?>

		<?php if ( have_rows( 'testimonials' ) ): ?>
			<div class="centered">
				<div class="testimonial__slider" data-component="testimonial">
					<?php while ( have_rows( 'testimonials' ) ): the_row();
						$content = get_sub_field( 'testimonial_content' );
						$name    = get_sub_field( 'testimonial_name' );
						$role    = get_sub_field( 'testimonial_role' );

						if ( ! empty( $content ) || ! empty( $name ) || ! empty( $role ) ):?>
							<div class="testimonial__slider-slide">
								<?php echo $content;

								if ( ! empty( $name ) || ! empty( $role ) ):?>
									<h4>
										<?php echo $name; ?>
										<br>
										<?php echo $role; ?>
									</h4>
								<?php endif; ?>
							</div>
						<?php endif;
					endwhile; ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
<?php endif; ?>