<?php
/**
 * The template part for displaying content slider
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$title   = get_sub_field( 'content_slider_title' );
$button  = get_sub_field( 'content_slider_button' );
$content = get_sub_field( 'content_slider_content' );

if ( ( have_rows( 'content_slider' ) ) || ! empty( $title ) || ! empty( $button ) || ! empty( $content ) ):?>
	<div class="content-slider centered centered--large">

		<?php if ( ! empty( $title ) || ! empty( $button ) || ! empty( $content ) ): ?>
			<div class="content-slider__info">
				<?php if ( ! empty( $title ) ): ?>
					<h3><?php echo $title; ?></h3>
				<?php endif;

				echo $content;

				if ( ! empty( $button ) ):?>
					<a class="button button--help" href="<?php echo $button['url']; ?>"
					   target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
				<?php endif; ?>

			</div>
		<?php endif;

		if ( have_rows( 'content_slider' ) ): ?>
			<div class="content-slider__list" data-component="content-slider">
				<?php while ( have_rows( 'content_slider' ) ) : the_row();

					$image   = get_sub_field( 'content_slide_image' );
					$title   = get_sub_field( 'content_slide_title' );
					$content = get_sub_field( 'content_slide_content' );

					if ( ! empty( $image ) || ! empty( $title ) || ! empty( $content ) ):?>
						<div class="content-slider__slide">

							<?php if ( ! empty( $image ) ): ?>
								<figure><?php echo wp_get_attachment_image( $image['id'], 'content-slide' ); ?></figure>
							<?php endif; ?>
							<div class="content-slider__slide-content">
								<?php if ( ! empty( $title ) ): ?>
									<h4><?php echo $title; ?></h4>
								<?php endif;

								echo $content; ?>
							</div>
						</div>
					<?php endif;
				endwhile; ?>
			</div>
		<?php endif; ?>

	</div>
<?php endif; ?>