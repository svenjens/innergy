<?php
/**
 * The template part for displaying about content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$form_shortcode = get_sub_field( 'form_shortcode' );
$content        = get_sub_field( 'content_d' );
if ( ! empty( $form_shortcode ) || ! empty( $content ) ) : ?>
	<div class="form-content centered">

		<?php if ( ! empty( $form_shortcode ) ) : ?>
			<div class="form-content__form">
				<?php echo $form_shortcode; ?>
			</div>
		<?php endif;

		if ( ! empty( $content ) ) : ?>
			<div class="form-content__content">
				<?php echo $content; ?>
			</div>
		<?php endif; ?>

	</div>
<?php endif; ?>