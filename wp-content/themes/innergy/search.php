<?php
/**
 * The template for displaying search results pages
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
	<h1><?php _t( 'Zoekresultaten voor: &ldquo;%s&rdquo;', [ get_search_query() ] ); ?></h1>

	<?php while ( have_posts() ) : the_post(); ?>
		<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
	<?php endwhile; ?>
<?php else : ?>
	<p><?php _t( 'We hebben helaas geen resultaten voor de door u ingetypte zoekterm kunnen vinden.' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
