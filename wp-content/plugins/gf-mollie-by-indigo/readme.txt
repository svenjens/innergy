=== GF Mollie by Indigo ===
Contributors: indigonl
Tags: gravity forms, ideal, bancontact, mollie
Requires at least: 4.9.7
Tested up to: 5.2.4
Stable tag: 1.10.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

You can link Mollie to Gravity Forms with GF Mollie by Indigo. 

== Description ==

You can link Mollie to Gravity Forms with GF Mollie by Indigo. Create order forms for workshops, products, or services and allow clients to pay online with iDEAL or another payment method.
Forms from Gravity Forms with accepted payment methods offer you a simple means of selling without the hassle of a webshop.
All payment methods offered by Mollie are available. In addition to iDEAL, these include Bancontact (Belgium), Sofort Banking (Germany, among others), credit cards, Bitcoins and more.

Key benefits
Link every payment method offered by Mollie to forms from Gravity Forms.
Link different notification emails to different payment statuses.
Use all Gravity Forms licence variants
No subscription fees, only Mollie transaction costs for orders 
Free updates and support

WordPress plugin required
Gravity Forms
Author: Rocketgenius
Website: https://www.gravityforms.com/
Requires at least version: 2.2.5
Tested till version: 2.4.14

Payment methods supported
iDEAL, Mastercard, Visa, American Express, Bancontact, PayPal, SEPA Direct Debit, SEPA Transfer, Gift Vouchers, KBC/CBC Payment Button, Belfius, Bitcoin, SOFORT Banking, paysafecard, CartaSi, Cartes Bancaires.



== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. In the following steps, we assume you have already obtained and installed the Gravity Forms plugin and that you already have a Mollie account.
4. Add product: go to your WordPress dashboard and create a form in Gravity Forms. Create the fields you need and at least one price field, for example, a product.
5. Applying VAT:add a product and set ‘Field type’ to ‘Calculation’. This will enable you to calculate VAT for other products.
6. Total amount:Select the ‘Total’ field from the ‘Price fields’ section. This will show the total amount outstanding on your form and will be used for paying.
7. Payment method:To enable your client to pay online, you create a field with a dropdown or radio buttons with a ‘Payment method’ label. Add a number of choices, e.g. Mollie and PayPal. It is up to you how you describe the Mollie payment method on your form: you can use Mollie or iDEAL / BanContact / Sofort Banking or similar, depending on the payment methods you are offering.
8. Install Mollie:On your new form, go to Settings -> GF Mollie by Indigo and select ‘Add new (feed)’. Select your title, enter the Mollie API key and the type of transaction: Products and Services.
In Payment Amount, select the total amount payable from the form.
Select the proviso in the last section. This could be something like: Process this feed as per all ‘Payment method = Mollie’
9. Set form confirmation:In your Form Settings, go to Confirmations, and for Standard Confirmation, enter something like: Thank you very much for your order! Check your mailbox for further information.
10. Set notifications:In your Form Settings, go to Notifications and create multiple Notifications:
One for order form sent (for admin)
One for Mollie payment failed (for admin + client)
One for Mollie payment successful (for admin + client)
Set the notification for the client and enter your own email address in bcc. That is the easiest, but you are free to create other notifications.
11. Test:You can now place the form in a page and test it. Make sure you use Mollie’s API test key first.
12. Go live: If everything works correctly and you are happy with the notifications and email messages, you can replace Mollie’s API key with the live key and start using the form.




== Screenshots ==

== Changelog ==

=1.10.1=
* Fixed merge tag visibility bug

=1.10.0=
* Changed version number

=1.0.9=
* Added a custom merge tag {gf-mollie-payment-status} that can be used with the Gravity Forms Conditional Shortcode

* Added a gf_mollie_request_args filter

=1.0.8=
* Added filter for dynamic Mollie description

=1.0.6=
* Members plugin integration

=1.0.5=
* Dont trigger emails on refunds

=1.0.4=
* Added general feed settings input

= 1.0.3 =
* incompatability fixes and mollie api update

= 1.0.2 =
* fixed php 7.1 compatibility

= 1.0 =
* first release

